package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ISenderService {

    void send(@NotNull String message);
}
