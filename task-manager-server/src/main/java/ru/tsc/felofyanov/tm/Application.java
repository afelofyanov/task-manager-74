package ru.tsc.felofyanov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.felofyanov.tm.component.Bootstrap;
import ru.tsc.felofyanov.tm.configuration.ServerConfiguration;

public final class Application {

    public static void main(@Nullable String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run();
    }
}
