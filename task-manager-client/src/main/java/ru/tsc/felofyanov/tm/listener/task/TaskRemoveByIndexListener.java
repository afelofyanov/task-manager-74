package ru.tsc.felofyanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.TaskRemoveByIndexRequest;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIndexListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by index.";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIndexListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(getToken(), index);
        getTaskEndpoint().removeTaskByIndex(request);
    }
}
