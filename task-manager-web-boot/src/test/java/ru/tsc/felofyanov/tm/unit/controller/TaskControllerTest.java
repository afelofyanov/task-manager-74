package ru.tsc.felofyanov.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.felofyanov.tm.marker.BootUnitCategory;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.service.TaskService;
import ru.tsc.felofyanov.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WebAppConfiguration
@Category(BootUnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TaskControllerTest {

    @Autowired
    private TaskService taskService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskService.create(UserUtil.getUserId());
        taskService.create(UserUtil.getUserId());
    }

    @After
    public void clean() {
        taskService.clear(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @NotNull final String url = "/task/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final List<Task> test = taskService.findAll(UserUtil.getUserId());
        Assert.assertNotNull(test);
        Assert.assertEquals(3, test.size());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final Task test = new Task(UserUtil.getUserId(), "Test");
        @NotNull final String url = "/task/delete/" + test.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertNull(taskService.findFirstByUserIdAndId(UserUtil.getUserId(), test.getId()));
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final Task test = new Task(UserUtil.getUserId(), "Test");
        @NotNull final String url = "/task/edit/" + test.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }
}
