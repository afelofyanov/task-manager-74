package ru.tsc.felofyanov.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.felofyanov.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.felofyanov.tm.client.AuthSoapEndpointClient;
import ru.tsc.felofyanov.tm.client.TaskSoapEndpointClient;
import ru.tsc.felofyanov.tm.marker.BootIntegrationCategory;
import ru.tsc.felofyanov.tm.model.Task;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

@Category(BootIntegrationCategory.class)
public class TaskSoapEndpointTest {

    @NotNull
    private static final String ROOT = "http://localhost:8080";

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static ITaskEndpoint taskEndpoint;

    @NotNull
    private final Task alpha = new Task("Test-1");

    @NotNull
    private final Task beta = new Task("Test-2");

    @NotNull
    private final Task gamma = new Task("Test-3");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(ROOT);
        Assert.assertTrue(authEndpoint.login("test", "test").getSuccess());
        taskEndpoint = TaskSoapEndpointClient.getInstance(ROOT);

        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) taskEndpoint;
        @NotNull Map<String, List<String>> headers = CastUtils.cast(
                (Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS)
        );
        if (headers == null) headers = new HashMap<String, List<String>>();

        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Before
    public void init() {
        taskEndpoint.save(alpha);
        taskEndpoint.save(beta);
    }

    @After
    public void clear() {
        taskEndpoint.clear();
    }

    @Test
    public void saveTest() {
        taskEndpoint.save(gamma);
        @Nullable final Task findTask = taskEndpoint.findById(gamma.getId());
        Assert.assertNotNull(findTask);
        Assert.assertEquals(gamma.getId(), findTask.getId());
    }

    @Test
    public void saveAllTest() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(alpha);
        tasks.add(gamma);
        taskEndpoint.saveAll(tasks);
        @Nullable final List<Task> findTask = taskEndpoint.findAll();
        Assert.assertNotNull(findTask);
        Assert.assertEquals(3, findTask.size());
    }

    @Test
    public void findAllTest() {
        @Nullable final List<Task> test = taskEndpoint.findAll();
        Assert.assertNotNull(test);
        Assert.assertEquals(2, test.size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final Task test = taskEndpoint.findById(beta.getId());
        Assert.assertNotNull(test);
        Assert.assertEquals(beta.getId(), test.getId());
    }

    @Test
    public void deleteTest() {
        taskEndpoint.delete(alpha);
        @Nullable final Task test = taskEndpoint.findById(alpha.getId());
        Assert.assertNull(test);
    }

    @Test
    public void deleteAllTest() {
        @NotNull final List<Task> tasks = new ArrayList<>();

        tasks.add(alpha);
        tasks.add(gamma);
        taskEndpoint.saveAll(tasks);

        Assert.assertEquals(3, taskEndpoint.findAll().size());
        taskEndpoint.deleteAll(tasks);
        Assert.assertEquals(1, taskEndpoint.findAll().size());
    }

    @Test
    public void deleteByIdTest() {
        taskEndpoint.deleteById(alpha.getId());
        @Nullable final Task test = taskEndpoint.findById(alpha.getId());
        Assert.assertNull(test);
        Assert.assertEquals(1, taskEndpoint.findAll().size());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, taskEndpoint.count());
        Assert.assertEquals(taskEndpoint.count(), taskEndpoint.findAll().size());
    }

    @Test
    public void existByIdTest() {
        Assert.assertTrue(taskEndpoint.existsById(alpha.getId()));
        Assert.assertFalse(taskEndpoint.existsById(gamma.getId()));
    }
}
