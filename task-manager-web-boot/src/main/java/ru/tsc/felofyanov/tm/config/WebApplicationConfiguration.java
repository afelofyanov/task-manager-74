package ru.tsc.felofyanov.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.tsc.felofyanov.tm.endpoint.AuthEndpoint;
import ru.tsc.felofyanov.tm.endpoint.ProjectEndpointImpl;
import ru.tsc.felofyanov.tm.endpoint.TaskEndpointImpl;

import javax.xml.ws.Endpoint;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebApplicationConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private Bus bus;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/services/*").permitAll()
                .antMatchers("/api/auth/*").permitAll()

                .and()
                .authorizeRequests()
                .anyRequest().authenticated()

                .and()
                .formLogin()
                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/login")
                .and()
                .csrf().disable();
    }

    @Override
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public Endpoint authEndpointRegistry(@NotNull final AuthEndpoint authEndpoint, @NotNull final SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, authEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint projectEndpointRegistry(@NotNull final ProjectEndpointImpl projectEndpoint, @NotNull final Bus bus) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(@NotNull final TaskEndpointImpl taskEndpoint, @NotNull final Bus bus) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }
}
