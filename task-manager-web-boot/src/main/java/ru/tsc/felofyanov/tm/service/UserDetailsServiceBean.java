package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.api.repository.IUserRepository;
import ru.tsc.felofyanov.tm.exception.RoleEmptyException;
import ru.tsc.felofyanov.tm.model.CustomUser;
import ru.tsc.felofyanov.tm.model.Role;
import ru.tsc.felofyanov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private IUserRepository repository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(@NotNull final String userName) throws UsernameNotFoundException {
        @Nullable final User user = repository.findFirstByLogin(userName);
        if (user == null) throw new UsernameNotFoundException(userName);

        @Nullable final List<Role> userRoles = user.getRoles();
        if (userRoles == null) throw new RoleEmptyException();
        @NotNull final List<String> roles = new ArrayList<>();
        for (final Role role : userRoles) roles.add(role.toString());

        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(userName)
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }
}
