package ru.tsc.felofyanov.tm.api.repository;

import ru.tsc.felofyanov.tm.model.Project;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

}
