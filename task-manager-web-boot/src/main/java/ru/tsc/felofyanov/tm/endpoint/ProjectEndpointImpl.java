package ru.tsc.felofyanov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.service.ProjectService;
import ru.tsc.felofyanov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.tsc.felofyanov.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @Autowired
    private ProjectService service;

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return service.count(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project")
            @RequestBody Project project
    ) {
        service.removeByUserIdAndEntity(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @WebParam(name = "projects")
            @RequestBody List<Project> projects
    ) {
        service.remove(UserUtil.getUserId(), projects);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        service.clear(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) {
        service.removeByByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) {
        return service.existsById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return service.findAll(UserUtil.getUserId()).stream().collect(Collectors.toList());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Project findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) {
        return service.findFirstByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Project save(
            @WebParam(name = "project")
            @RequestBody Project project
    ) {
        return service.save(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @PostMapping("/saveAll")
    public List<Project> saveAll(
            @WebParam(name = "projects")
            @RequestBody List<Project> projects
    ) {
        return service.save(UserUtil.getUserId(), projects);
    }
}
