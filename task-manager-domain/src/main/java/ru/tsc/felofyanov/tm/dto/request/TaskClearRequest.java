package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskClearRequest extends AbstractUserRequest {

    public TaskClearRequest(@Nullable String token) {
        super(token);
    }
}
