package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataSaveXmlJaxbRequest extends AbstractUserRequest {

    public DataSaveXmlJaxbRequest(@Nullable String token) {
        super(token);
    }
}
